<?php

namespace App\Http\Controllers;

use App\Admin;

use View;
use Auth;
use Lang;
use Input;
use Redirect;
use Response;
use Validator;

class AuthController extends Controller {


	public function getLogin() {
		if (Auth::guard("admin")->check())
			return Redirect::route("admin.dashboard");

		return View::make("admin.login");
	}


	public function postLogin() {
		$validator = Validator::make(Input::all(), [
			"email" => "required|email",
			"password" => "required|string",
			"remember" => "sometimes|boolean",
		]);
		if ($validator->passes()) {
			$passed = Auth::guard("admin")->attempt([
					'email' => Input::get("email"),
					'password' => Input::get("password")
				], Input::get("remember", false));
			
			if ($passed)
				return Redirect::route("admin.index")->withSuccess(Lang::get("notifications.success.login"));
			else
				return Redirect::back()->withErrors(Lang::get("notifications.error.login"))->withInput();
		}

		return Redirect::back()->withErrors($validator->errors())->withInput();
	}


	public function getLogout() {
		if (Auth::guard("admin")->check()) {
			Auth::guard("admin")->logout();
			return Redirect::route("admin.login.get")->withSuccess(Lang::get("notifications.success.logout"));
		}
		else
			return Redirect::back()->withErrors(Lang::get("notifications.error.logout"));
	}
}
