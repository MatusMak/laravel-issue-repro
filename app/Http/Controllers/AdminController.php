<?php

namespace App\Http\Controllers;

use View;

class AdminController extends Controller
{
	public function getIndex() {
		return View::make("admin.index");
	}
}
