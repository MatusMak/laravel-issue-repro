<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App;
use URL;
use Schema;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->setSchemaStringLength();
        $this->setCliUrl();
    }

    /**
     * Setting default string length to 191 allows us
     * to use unique indexes for strings in MariaDB
     *
     * @return void
     */
    private function setSchemaStringLength() {
        Schema::defaultStringLength(191);
    }


    /**
     * Ensures that CLI is using correct root application URL
     * 
     * @return void
     */
    private function setCliUrl() {
        if (App::runningInConsole())
            URL::forceRootUrl(config("app.url"));
    }
}
