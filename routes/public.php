<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(["prefix" => "admin/auth"], function() {

	Route::get("/login", "AuthController@getLogin")->name("admin.login.get");
	Route::post("/login", "AuthController@postLogin")->name("admin.login.post");

	Route::get("/logout", "AuthController@getLogout")->name("admin.logout");

});

