<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin;
        $admin->name = "test";
        $admin->email = "admin@example.com";
        $admin->password = "password";
        $admin->save();

        $user = new User;
        $user->name = "test";
        $user->email = "user@example.com";
        $user->password = "password";
        $user->save();
    }
}
